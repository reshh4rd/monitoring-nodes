from flask import Flask, request, jsonify
import json
import os
import subprocess

app = Flask(__name__)

FILE_PATH = "/data/monitoring-nodes/monitoring_server/prometheus/file_sd/node_targets.json"

@app.route('/webhook/add', methods=['POST'])
def webhook_add():
    data = request.get_json()
    if not data:
        return jsonify({"error": "Invalid JSON"}), 400

    target_entry = {
        "targets": [f"{data['IP']}:9100"],
        "labels": {
            "name": data['name'],
            "IP": data['IP'],
            "Nodes": data['Nodes'],
            "Hosting": data['Hosting'],
            "Location": data['Location'],
            "Price": data['Price']
        }
    }

    if not os.path.exists(FILE_PATH):
        file_data = []
    else:
        with open(FILE_PATH, 'r') as f:
            try:
                file_data = json.load(f)
            except json.JSONDecodeError:
                file_data = []

    file_data.append(target_entry)

    with open(FILE_PATH, 'w') as f:
        json.dump(file_data, f, indent=4)

    subprocess.run(["docker", "exec", "-ti", "prometheus", "kill", "-HUP", "1"])
    return jsonify({"status": "add success"}), 200

@app.route('/webhook/del', methods=['POST'])
def webhook_remove():
    data = request.get_json()
    if not data or 'IP' not in data:
        return jsonify({"error": "Invalid request, IP address not provided"}), 400

    if os.path.exists(FILE_PATH):
        with open(FILE_PATH, 'r') as f:
            try:
                file_data = json.load(f)
            except json.JSONDecodeError:
                return jsonify({"error": "Failed to decode JSON data from file"}), 500

        filtered_data = [entry for entry in file_data if entry['labels']['IP'] != data['IP']]

        with open(FILE_PATH, 'w') as f:
            json.dump(filtered_data, f, indent=4)
            
        subprocess.run(["docker", "exec", "-ti", "prometheus", "kill", "-HUP", "1"])
        return jsonify({"status": "delete success"}), 200
    else:
        return jsonify({"error": "File not found"}), 404


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1488)

