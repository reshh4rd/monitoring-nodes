import requests
import json

ip_server_nenosika = '65.109.165.249'

action = 'add' # add or del

if action == 'add': # add
    data = {
        "name": "Farcaster",
        "IP": "95.217.135.146",
        "Nodes": "Farcaster",
        "Hosting": "Hetzner",
        "Location": "Helsinki",
        "Price": "7.05"
    }

if action == 'del': # del
    data = {"IP": "95.217.0.49"}

headers = {
    'Content-Type': 'application/json'
}

url = f'http://{ip_server_nenosika}:1488/webhook/{action}'
response = requests.post(url, data=json.dumps(data), headers=headers)

if response.status_code == 200:
    print('Request was successful')
    print('Response:', response.json())
else:
    print('Request failed')
    print('Status code:', response.status_code)
    print('Response:', response.text)
